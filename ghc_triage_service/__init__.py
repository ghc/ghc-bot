import gitlab
import logging
from wsgiref.simple_server import make_server
from gitlab_webhook import WebhookServer

NEW_ISSUE_LABEL = 'needs triage'
NEW_MERGE_REQUEST_LABEL = 'needs triage'

class GHCWebhookServer(WebhookServer):
    def __init__(self, port: int, gl: gitlab.Gitlab):
        WebhookServer.__init__(self, port)
        self.gl = gl

    def handle_merge_request_event(self, event) -> None:
        attrs = event['object_attributes']

        if attrs.get('action') != 'open':
            return

        proj_id = event['project']['id']
        labels = event['labels']
        if len(labels) == 0:
            iid = attrs['iid']
            logging.info(f'Adding label {NEW_ISSUE_LABEL} to merge request #{iid} of project {proj_id}')
            project = self.gl.projects.get(proj_id)
            mr = project.merge_requests.get(iid)
            mr.labels.append(NEW_MERGE_REQUEST_LABEL)
            mr.save()

    def handle_issue_event(self, event) -> None:
        attrs = event['object_attributes']

        if attrs.get('action') != 'open':
            return

        proj_id = event['project']['id']
        labels = event['labels']
        if len(labels) == 0:
            iid = attrs['iid']
            logging.info(f'Adding label {NEW_ISSUE_LABEL} to issue #{iid} of project {proj_id}')
            project = self.gl.projects.get(proj_id)
            issue = project.issues.get(iid)
            issue.labels.append(NEW_ISSUE_LABEL)
            issue.save()

def main() -> None:
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--gitlab-root', type=str, help='GitLab root URL')
    parser.add_argument('--access-token', type=str, help='GitLab access token for bot user')
    parser.add_argument('--port', type=int, required=True, help='Listen port')
    parser.add_argument('--verbose', action='store_true', help='Enable verbose output')
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    gl = gitlab.Gitlab(args.gitlab_root, args.access_token)
    server = GHCWebhookServer(args.port, gl)
    server.run()
