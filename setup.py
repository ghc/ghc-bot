from setuptools import setup

setup(
    name='gitlab-triage-label-service',
    version='0.1',
    packages=['gitlab_webhook', 'ghc_triage_service', 'test_tracking'],
    install_requires=['falcon', 'python-gitlab', 'asyncpg'],
    entry_points = {
        'console_scripts': [
            'gitlab-triage-label-service = ghc_triage_service:main',
            'gitlab-ingest-junit = test_tracking.ingest_junit:main',
            'gitlab-test-tracking-service = test_tracking.webhook:main',
        ]
    }
)

