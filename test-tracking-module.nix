{ pkgs, config, lib, ... }:

let service = pkgs.python3Packages.callPackage ./package.nix {};
in
{
  options.services.ghc-test-tracking = {
    accessToken = lib.mkOption {
      type = lib.types.str;
    };
    listenPort = lib.mkOption {
      type = lib.types.int;
      default = 8087;
    };
  };

  config = {
    users.users.ghc-test-tracking = {
      description = "GitLab test tracking service";
      isSystemUser = true;
      group = "ghc-test-tracking";
    };
    users.groups.ghc-test-tracking = {};
      
    systemd.services.gitlab-test-tracking = {
      description = "GitLab test tracking service";
      after = [ "postgresql.service" ];
      requires = [ "postgresql.service" ];
      serviceConfig.User = "ghc-test-tracking";
      script = ''
        ${service}/bin/gitlab-test-tracking-service \
          --gitlab-root=https://gitlab.${config.ghc.rootDomain} \
          --access-token="${config.services.ghc-test-tracking.accessToken}" \
          --port=${toString config.services.ghc-test-tracking.listenPort} \
          --conn-string="postgresql:///ghc_test_tracking"
      '';
      wantedBy = [ "multi-user.target" ];
    };
  };
}
