#!/usr/bin/env python3

import gitlab
import logging
from . import ingest_junit
from gitlab_webhook import WebhookServer
from xml.etree import ElementTree as ET
import asyncpg
import asyncio

class GHCCIWebhookServer(WebhookServer):
    def __init__(self, port: int, conn: asyncpg.Connection, gl: gitlab.Gitlab):
        WebhookServer.__init__(self, port)
        self.gl = gl
        self.conn = conn

    def handle_job_event(self, event) -> None:
        if event['build_status'] not in ['success', 'failed']:
            return

        proj_id = event['project_id']
        job_id = event['build_id']
        logging.info(f"project {proj_id} job {job_id} finished")
        self.ingest_job(proj_id, job_id)

    def ingest_job(self, project_id: int, job_id: int) -> None:
        job = self.gl.projects.get(project_id).jobs.get(job_id)
        job_name = job.name
        sha = job.pipeline['sha']
        junit_files = [ a for a in job._attrs['artifacts'] if a['file_type'] == 'junit' ]

        if len(junit_files) == 0:
            logging.info(f'Job {job_id} ({job_name}, {sha}) has no JUnit output')
        else:
            fname = junit_files[0]['filename']
            if fname.endswith('.gz'):
                fname = fname[:-3] # WTF?
            logging.info(f'Job {job_id} ({job_name}, {sha}) has JUnit output {fname}')
            junit = job.artifact(fname, streamed=False)
            if junit is None:
                logging.info(f'Failed to fetch JUnit output for job {job_id}')
            else:
                # N.B. job.artifacts may return an Iterator if we had passed streamed=True
                assert isinstance(junit, bytes)
                parsed = ET.fromstring(junit.decode('UTF-8'))
                loop = asyncio.get_event_loop()
                loop.run_until_complete(ingest_junit.ingest(self.conn, job_name=job_name, commit_sha=sha, el=parsed))

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--gitlab-root', type=str,
                        help='GitLab root URL')
    parser.add_argument('--access-token', type=str,
                        help='GitLab access token for bot user')
    parser.add_argument('-c', '--conn-string', type=str,
                        default=ingest_junit.DEFAULT_CONN_STRING,
                        help='Connection string')
    parser.add_argument('-p', '--port', type=int, required=True,
                        help='Listen port')
    parser.add_argument('--test', type=int, metavar='PROJ JOB', nargs=2,
                        help='Test against job ID')
    parser.add_argument('--verbose', action='store_true',
                        help='Enable verbose output')
    args = parser.parse_args()

    level = logging.DEBUG if args.verbose else logging.INFO
    logging.basicConfig(level=level)

    gl = gitlab.Gitlab(args.gitlab_root, args.access_token)
    loop = asyncio.get_event_loop()
    conn = loop.run_until_complete(asyncpg.connect(args.conn_string))
    server = GHCCIWebhookServer(args.port, conn, gl)
    if args.test is not None:
        proj_id, job_id = args.test
        loop.run_until_complete(server.ingest_job(proj_id, job_id))
    else:
        server.run()
    
