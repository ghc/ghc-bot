#!/usr/bin/env python3

import json
import logging
import typing
from datetime import datetime
from xml.etree import ElementTree as ET
import asyncpg
import asyncio

DEFAULT_CONN_STRING = 'postgresql:///ghc_test_tracking'

async def get_test_id(conn: asyncpg.Connection, name: str) -> int:
    res = await conn.fetchrow("SELECT id FROM tests WHERE name = $1", name)
    if res is None:
        res = await conn.fetchrow("INSERT INTO tests (name) VALUES ($1) RETURNING id", name)

    return res[0]

async def ingest_testcase(conn: asyncpg.Connection, test_run_id: int, case: ET.Element) -> None:
    name = case.attrib['name']
    failure = case.find('failure')
    other = {}
    if failure is not None:
        message = failure.attrib['message']
        fail_type = failure.attrib['type']
        if fail_type == 'unexpected failure':
            result = 'unexpected_failure'
        elif fail_type == 'stat failure':
            result = 'unexpected_failure'
        elif fail_type == 'unexpected pass':
            result = 'unexpected_pass'
        elif fail_type == 'fragile pass':
            result = 'fragile_pass'
        elif fail_type == 'fragile failure':
            result = 'fragile_failure'
        # Old convention:
        elif fail_type == 'fragile':
            if message == 'fragile pass':
                result = 'fragile_pass'
            elif message == 'fragile failure':
                result = 'fragile_failure'
            else:
                logging.info('unexpected fragile test message: %s' % message)
        else: 
            logging.info('unexpected failure type: %s' % fail_type)
            return

        other['message'] = message
    else:
        result = 'success'

    test_id = await get_test_id(conn, name)
    await conn.execute("""
          INSERT INTO test_results
            (test_run_id, test_id, result, other)
            VALUES ($1, $2, $3, $4)
        """, test_run_id, test_id, result, json.dumps(other))

async def ingest(
        conn: asyncpg.Connection,
        job_name: str, commit_sha: str,
        el: ET.Element) -> None:
    async with conn.transaction():
        n = 0
        for testsuite in el.iterfind('testsuite'):
            time = datetime.fromisoformat(testsuite.attrib['timestamp'])
            test_run_id, = await conn.fetchrow("""
                  INSERT INTO test_runs
                    (time, job_name, commit_sha)
                    VALUES ($1, $2, $3)
                    RETURNING id
                """, time, job_name, commit_sha)

            for case in testsuite.iterfind('testcase'):
                n += 1
                await ingest_testcase(conn, test_run_id, case)

        logging.info(f"Ingested {n} testcase results for job {job_name} on {commit_sha} (test run id {test_run_id})")

async def run(conn_string: str, junit_file: typing.TextIO) -> None:
    job_name = 'test'
    commit_sha = 'test'

    parsed = ET.parse(junit_file)
    conn = await asyncpg.connect(conn_string)
    await ingest(conn, job_name, commit_sha, parsed.getroot())

def main() -> None:
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--conn-string', type=str,
                        default=DEFAULT_CONN_STRING,
                        help='Connection string')
    parser.add_argument('file', type=argparse.FileType('r'), help='JUnit output')
    args = parser.parse_args()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(args.conn_string, args.file))

if __name__ == "__main__":
    main()

