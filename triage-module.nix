{ pkgs, config, lib, ... }:

let service = pkgs.python3Packages.callPackage ./package.nix {};
in
{
  options.services.ghc-triage = {
    accessToken = lib.mkOption {
      type = lib.types.str;
    };
    listenPort = lib.mkOption {
      type = lib.types.int;
      default = 8085;
    };
  };

  config = {
    systemd.services.gitlab-triage-label-service = {
      description = "GitLab label triage service";
      script = ''
        ${service}/bin/gitlab-triage-label-service \
          --gitlab-root=https://gitlab.${config.ghc.rootDomain} \
          --access-token="${config.services.ghc-triage.accessToken}" \
          --port=${toString config.services.ghc-triage.listenPort}
      '';
      wantedBy = [ "multi-user.target" ];
      serviceConfig.DynamicUser = true;
    };
  };
}
