CREATE DATABASE ghc_test_tracking;
\c ghc_test_tracking

CREATE USER "ghc-test-tracking";
GRANT SELECT, INSERT ON ALL TABLES IN SCHEMA public TO "ghc-test-tracking";
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO "ghc-test-tracking";

CREATE TYPE result_type AS ENUM 
  ('success', 'unexpected_failure', 'unexpected_pass', 'fragile_failure', 'fragile_pass');
  
CREATE TABLE tests
  ( id serial PRIMARY KEY
  , name text UNIQUE NOT NULL
  );

CREATE TABLE test_runs
  ( id serial PRIMARY KEY
  , time timestamp NOT NULL
  , job_name text NOT NULL
  , commit_sha text NOT NULL
  );

CREATE TABLE test_results 
  ( test_run_id integer REFERENCES test_runs(id)
  , test_id integer REFERENCES tests(id)
  , result result_type NOT NULL
  , other jsonb
  , UNIQUE (test_run_id, test_id)
  );

CREATE INDEX test_results_test_id_idx ON test_results(test_id);

CREATE VIEW results_view AS
  SELECT   job_name
         , tests.name as test_name
         , time
         , commit_sha
         , result
         , other
  FROM test_results
  INNER JOIN test_runs ON test_results.test_run_id = test_runs.id
  INNER JOIN tests ON test_results.test_id = tests.id;
