{
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";
  outputs = inputs@{ self, nixpkgs, ... }: {
    packages.x86_64-linux.default =
      let pkgs = nixpkgs.legacyPackages.x86_64-linux;
      in pkgs.python3Packages.callPackage ./package.nix {};
    nixosModules.ghc-triage = import ./triage-module.nix;
    nixosModules.ghc-test-tracking = import ./test-tracking-module.nix;
  };
}
