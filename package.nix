{ buildPythonApplication, isPy3k, python-gitlab, falcon, psycopg2, mypy, asyncpg }:

buildPythonApplication rec {
  pname = "gitlab-triage-label-service";
  version = "0.1";
  src = ./.;
  propagatedBuildInputs = [ python-gitlab falcon psycopg2 asyncpg ];
  disabled = !isPy3k;
  checkInputs = [ mypy ];
  checkPhase = ''
    echo $PATH
    echo ${mypy}
    ${mypy}/bin/mypy --ignore-missing-imports . --exclude build
  '';
}
